/*
 Navicat Premium Data Transfer

 Source Server         : 阿里突发1-1数据库
 Source Server Type    : MariaDB
 Source Server Version : 50565
 Source Host           : db.fangjc1986.com:3306
 Source Schema         : ezmpro_copy

 Target Server Type    : MariaDB
 Target Server Version : 50565
 File Encoding         : 65001

 Date: 25/05/2020 16:17:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_account_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_account_log`;
CREATE TABLE `sys_account_log`  (
  `id` bigint(20) NOT NULL COMMENT '雪花主键',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `type` tinyint(255) NULL DEFAULT NULL COMMENT '类型：1：登录；2：登出',
  `success` tinyint(255) NULL DEFAULT NULL COMMENT '是否成功',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '编辑时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编辑人',
  `tenant_id` bigint(20) NULL DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账号登录登出日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth`;
CREATE TABLE `sys_auth`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `type` tinyint(4) NULL DEFAULT 1 COMMENT '类型：1：页面；2：API；',
  `icon` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父id',
  `category` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  `rank` int(255) NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_auth
-- ----------------------------
INSERT INTO `sys_auth` VALUES (1, '首页', '/system/index', 1, 'icon-home', 0, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:06:11', '2020-03-30 14:31:18', NULL, 1);
INSERT INTO `sys_auth` VALUES (2, '基础配置', '/base', 1, 'icon-cogs', 0, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:19:33', '2020-03-27 17:12:06', NULL, 2);
INSERT INTO `sys_auth` VALUES (3, '字典配置', '/base/dict', 1, 'icon-book', 2, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:11:41', '2020-03-30 14:30:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (4, '系统参数', '/base/config', 1, 'icon-sitemap', 2, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:12:03', '2020-03-30 14:31:27', NULL, 0);
INSERT INTO `sys_auth` VALUES (5, '用户权限', '/user_power', 1, 'icon-user-shield', 0, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:17:01', '2020-03-30 14:31:32', NULL, 3);
INSERT INTO `sys_auth` VALUES (6, '用户管理', '/user_power/user', 1, 'icon-user', 5, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:18:31', '2020-03-30 14:31:38', NULL, 1);
INSERT INTO `sys_auth` VALUES (7, '角色管理', '/user_power/role', 1, 'icon-user-tag', 5, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:19:09', '2020-03-30 14:31:42', NULL, 2);
INSERT INTO `sys_auth` VALUES (8, '菜单权限', '/user_power/auth', 1, 'icon-user-shield', 5, 'PC', NULL, 'admin', 'admin', '2020-03-19 10:19:33', '2020-03-30 14:31:46', NULL, 3);
INSERT INTO `sys_auth` VALUES (1244893434272083970, '隔离组（租户）', '/user_power/tenant', 1, 'icon-map-signs', 5, 'PC', '', 'admin', 'admin', '2020-03-31 15:44:57', '2020-03-31 15:45:54', NULL, 4);
INSERT INTO `sys_auth` VALUES (1248057803618082817, '权限-详情', '/sys/auth/get', 2, 'icon-folder1', 8, 'PC', '', 'admin', 'admin', '2020-04-09 09:19:02', '2020-04-09 09:19:40', NULL, 0);
INSERT INTO `sys_auth` VALUES (1248058106190979073, '权限-列表', '/sys/auth/list', 2, 'icon-folder1', 8, 'PC', '', 'admin', 'admin', '2020-04-09 09:20:14', '2020-04-09 09:20:20', NULL, 0);
INSERT INTO `sys_auth` VALUES (1248058247186702337, '权限-删除', '/sys/auth/remove', 2, 'icon-folder1', 8, 'PC', '', 'admin', NULL, '2020-04-09 09:20:48', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248058416749830146, '权限-增改', '/sys/auth/save', 2, 'icon-folder1', 8, 'PC', '', 'admin', NULL, '2020-04-09 09:21:29', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248058504414978049, '权限-批改', '/sys/auth/updateList', 2, 'icon-folder1', 8, 'PC', '', 'admin', NULL, '2020-04-09 09:21:49', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248060545086480385, '系统参数-详情', '/sys/config/get', 2, 'icon-folder1', 4, 'PC', '', 'admin', NULL, '2020-04-09 09:29:56', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248060633355608066, '系统参数-列表', '/sys/config/list', 2, 'icon-folder1', 4, 'PC', '', 'admin', NULL, '2020-04-09 09:30:17', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248060723482812418, '系统参数-删除', '/sys/config/remove', 2, 'icon-folder1', 4, 'PC', '', 'admin', NULL, '2020-04-09 09:30:38', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248060795557732354, '系统参数-增改', '/sys/config/save', 2, 'icon-folder1', 4, 'PC', '', 'admin', NULL, '2020-04-09 09:30:56', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248060865019600897, '系统参数-批改', '/sys/config/updateList', 2, 'icon-folder1', 4, 'PC', '', 'admin', NULL, '2020-04-09 09:31:12', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061373381828610, '字典-详情', '/sys/dict/get', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:33:13', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061424950796290, '字典-列表', '/sys/dict/list', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:33:26', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061540122189826, '字典-删除', '/sys/dict/remove', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:33:53', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061604844494849, '字典-增改', '/sys/dict/save', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:34:09', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061685660344321, '字典-批改', '/sys/dict/updateList', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:34:28', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061837909385217, '字典明细-详情', '/sys/dict-detail/get', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:35:04', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061892020101122, '字典明细-列表', '/sys/dict-detail/list', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:35:17', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248061960609554434, '字典明细-删除', '/sys/dict-detail/remove', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:35:33', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248062044613074946, '字典明细-增改', '/sys/dict-detail/save', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:35:53', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248062162493988865, '字典明细-批改', '/sys/dict-detail/updateList', 2, 'icon-folder1', 3, 'PC', '', 'admin', NULL, '2020-04-09 09:36:22', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066208646782978, '角色-用户所属权限', '/sys/role/authList', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:52:26', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066339571982338, '角色-修改绑定权限', '/sys/role/bindAuths', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:52:57', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066409121931266, '角色-详情', '/sys/role/get', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:53:14', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066464939728898, '角色-列表', '/sys/role/list', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:53:27', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066517012013057, '角色-删除', '/sys/role/remove', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:53:40', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066563463929858, '角色-增改', '/sys/role/save', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:53:51', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066623975153665, '角色-批改', '/sys/role/updateList', 2, 'icon-folder1', 7, 'PC', '', 'admin', NULL, '2020-04-09 09:54:05', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066758067052546, '租户-详情', '/sys/tenant/get', 2, 'icon-folder1', 1244893434272083970, 'PC', '', 'admin', NULL, '2020-04-09 09:54:37', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066836609589250, '租户-列表', '/sys/tenant/list', 2, 'icon-folder1', 1244893434272083970, 'PC', '', 'admin', NULL, '2020-04-09 09:54:56', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066909967966210, '租户-删除', '/sys/tenant/remove', 2, 'icon-folder1', 1244893434272083970, 'PC', '', 'admin', NULL, '2020-04-09 09:55:13', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248066969862627329, '租户-增改', '/sys/tenant/save', 2, 'icon-folder1', 1244893434272083970, 'PC', '', 'admin', NULL, '2020-04-09 09:55:28', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067015702175745, '租户-批改', '/sys/tenant/updateList', 2, 'icon-folder1', 1244893434272083970, 'PC', '', 'admin', NULL, '2020-04-09 09:55:39', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067386604478466, '用户-详情', '/sys/user/get', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:57:07', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067455227486210, '用户-列表', '/sys/user/list', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:57:23', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067588979646465, '用户-删除', '/sys/user/remove', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:57:55', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067633779007490, '用户-增改', '/sys/user/save', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:58:06', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067715752484865, '用户-批改', '/sys/user/updateList', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:58:26', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248067836858818562, '用户-修改租户角色关系', '/sys/user/updateURT', 2, 'icon-folder1', 6, 'PC', '', 'admin', NULL, '2020-04-09 09:58:54', NULL, NULL, 0);
INSERT INTO `sys_auth` VALUES (1248438071659020289, '系统日志', '/log', 1, 'icon-clipboard-list', 0, 'PC', '', 'admin', 'admin', '2020-04-10 10:30:05', '2020-04-17 14:43:06', NULL, 100);
INSERT INTO `sys_auth` VALUES (1248438375238549506, '用户登录日志', '/log/account_log', 1, 'icon-user1', 1248438071659020289, 'PC', '', 'admin', 'admin', '2020-04-10 10:31:18', '2020-04-17 14:44:40', NULL, 1);
INSERT INTO `sys_auth` VALUES (1248438586342064130, '用户操作日志', '/log/request_log', 1, 'icon-pen-square', 1248438071659020289, 'PC', '', 'admin', 'admin', '2020-04-10 10:32:08', '2020-04-17 14:44:40', NULL, 2);
INSERT INTO `sys_auth` VALUES (1250946156956946433, '仓储 WMS (DEMO)', '/wms', 1, 'icon-warehouse', 0, 'PC', '', 'admin', 'admin', '2020-04-17 08:36:19', '2020-04-17 08:41:40', NULL, 7);
INSERT INTO `sys_auth` VALUES (1250946376805584897, '库房管理', '/wms/wrs', 1, 'icon-th', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-17 08:37:12', '2020-04-17 15:00:46', NULL, 1);
INSERT INTO `sys_auth` VALUES (1250946729915650049, '仓库', '/wms/warehouse', 1, 'icon-door-open', 1250946376805584897, 'PC', '', 'admin', 'admin', '2020-04-17 08:38:36', '2020-04-17 08:41:40', NULL, 1);
INSERT INTO `sys_auth` VALUES (1250946807938093058, '库位', '/wms/reservoir', 1, 'icon-grip-horizontal', 1250946376805584897, 'PC', '', 'admin', 'admin', '2020-04-17 08:38:55', '2020-04-17 08:41:40', NULL, 2);
INSERT INTO `sys_auth` VALUES (1250946903194931201, '货架', '/wms/shelf', 1, 'icon-cubes', 1250946376805584897, 'PC', '', 'admin', 'admin', '2020-04-17 08:39:17', '2020-04-17 08:41:40', NULL, 3);
INSERT INTO `sys_auth` VALUES (1251042062280654849, '基础数据', '/wms/jcsj', 1, 'icon-cog', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-17 14:57:25', '2020-04-17 15:00:46', NULL, 2);
INSERT INTO `sys_auth` VALUES (1251042189653278722, '合作伙伴', '/wms/company', 1, 'icon-user-friends', 1251042062280654849, 'PC', '', 'admin', 'admin', '2020-04-17 14:57:55', '2020-04-17 14:57:55', NULL, 1);
INSERT INTO `sys_auth` VALUES (1251042711470833666, '货品', '/wms/goods', 1, 'icon-cube', 1251042062280654849, 'PC', '', 'admin', 'admin', '2020-04-17 15:00:00', '2020-04-17 15:00:00', NULL, 2);
INSERT INTO `sys_auth` VALUES (1251056113387700225, '仓库-列表', '/wms/warehouse/list', 2, 'icon-folder1', 1250946729915650049, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:15', '2020-04-17 15:53:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056113429643265, '仓库-详情', '/wms/warehouse/get', 2, 'icon-folder1', 1250946729915650049, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:15', '2020-04-17 15:53:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056113429643266, '仓库-增改', '/wms/warehouse/save', 2, 'icon-folder1', 1250946729915650049, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:15', '2020-04-17 15:53:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056113429643267, '仓库-批改', '/wms/warehouse/updateList', 2, 'icon-folder1', 1250946729915650049, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:15', '2020-04-17 15:53:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056113429643268, '仓库-删除', '/wms/warehouse/remove', 2, 'icon-folder1', 1250946729915650049, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:15', '2020-04-17 15:53:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056274360893441, '库位-列表', '/wms/reservoir/list', 2, 'icon-folder1', 1250946807938093058, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:53', '2020-04-17 15:53:53', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056274365087745, '库位-详情', '/wms/reservoir/get', 2, 'icon-folder1', 1250946807938093058, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:53', '2020-04-17 15:53:53', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056274365087746, '库位-增改', '/wms/reservoir/save', 2, 'icon-folder1', 1250946807938093058, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:53', '2020-04-17 15:53:53', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056274373476353, '库位-批改', '/wms/reservoir/updateList', 2, 'icon-folder1', 1250946807938093058, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:53', '2020-04-17 15:53:53', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056274373476354, '库位-删除', '/wms/reservoir/remove', 2, 'icon-folder1', 1250946807938093058, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:53', '2020-04-17 15:53:53', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056287472287745, '货架-列表', '/wms/shelf/list', 2, 'icon-folder1', 1250946903194931201, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:57', '2020-04-17 15:53:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056287484870658, '货架-详情', '/wms/shelf/get', 2, 'icon-folder1', 1250946903194931201, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:57', '2020-04-17 15:53:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056287484870659, '货架-增改', '/wms/shelf/save', 2, 'icon-folder1', 1250946903194931201, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:57', '2020-04-17 15:53:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056287484870660, '货架-批改', '/wms/shelf/updateList', 2, 'icon-folder1', 1250946903194931201, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:57', '2020-04-17 15:53:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056287484870661, '货架-删除', '/wms/shelf/remove', 2, 'icon-folder1', 1250946903194931201, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:53:57', '2020-04-17 15:53:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056337954930689, '合作伙伴-列表', '/wms/company/list', 2, 'icon-folder1', 1251042189653278722, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:09', '2020-04-17 15:54:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056337959124994, '合作伙伴-详情', '/wms/company/get', 2, 'icon-folder1', 1251042189653278722, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:09', '2020-04-17 15:54:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056337959124995, '合作伙伴-增改', '/wms/company/save', 2, 'icon-folder1', 1251042189653278722, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:09', '2020-04-17 15:54:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056337959124996, '合作伙伴-批改', '/wms/company/updateList', 2, 'icon-folder1', 1251042189653278722, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:09', '2020-04-17 15:54:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056337967513602, '合作伙伴-删除', '/wms/company/remove', 2, 'icon-folder1', 1251042189653278722, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:09', '2020-04-17 15:54:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056348553936898, '货品-列表', '/wms/goods/list', 2, 'icon-folder1', 1251042711470833666, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:11', '2020-04-17 15:54:11', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056348553936899, '货品-详情', '/wms/goods/get', 2, 'icon-folder1', 1251042711470833666, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:11', '2020-04-17 15:54:11', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056348553936900, '货品-增改', '/wms/goods/save', 2, 'icon-folder1', 1251042711470833666, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:11', '2020-04-17 15:54:11', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056348562325506, '货品-批改', '/wms/goods/updateList', 2, 'icon-folder1', 1251042711470833666, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:11', '2020-04-17 15:54:11', NULL, 0);
INSERT INTO `sys_auth` VALUES (1251056348562325507, '货品-删除', '/wms/goods/remove', 2, 'icon-folder1', 1251042711470833666, 'PC', NULL, 'admin', 'admin', '2020-04-17 15:54:11', '2020-04-17 15:54:11', NULL, 0);
INSERT INTO `sys_auth` VALUES (1252464273684152322, '库存情况', '/inv', 1, 'icon-chalkboard', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-21 13:08:47', '2020-04-21 13:08:47', NULL, 3);
INSERT INTO `sys_auth` VALUES (1252464537463930881, '产品库存', '/wms/goods_inv', 1, 'icon-folder1', 1252464273684152322, 'PC', '', 'admin', 'admin', '2020-04-21 13:09:50', '2020-04-21 13:09:50', NULL, 1);
INSERT INTO `sys_auth` VALUES (1252464646419365889, '货架库存', '/wms/inventory', 1, 'icon-folder1', 1252464273684152322, 'PC', '', 'admin', 'admin', '2020-04-21 13:10:16', '2020-04-21 13:10:16', NULL, 2);
INSERT INTO `sys_auth` VALUES (1252465059818356737, '入库', '/wms_in', 1, 'icon-sign-in-alt', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-21 13:11:54', '2020-04-21 13:11:54', NULL, 4);
INSERT INTO `sys_auth` VALUES (1252465159240138754, '出库', '/wms_out', 1, 'icon-sign-out-alt', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-21 13:12:18', '2020-04-21 13:12:18', NULL, 5);
INSERT INTO `sys_auth` VALUES (1252465349346967553, '入库单', '/wms/receipt', 1, 'icon-clipboard-list', 1252465059818356737, 'PC', '', 'admin', 'admin', '2020-04-21 13:13:03', '2020-04-21 13:13:03', NULL, 1);
INSERT INTO `sys_auth` VALUES (1252465547934679042, '入库审批', '/wms/receipt_approve', 1, 'icon-clipboard-check', 1252465059818356737, 'PC', '', 'admin', 'admin', '2020-04-21 13:13:50', '2020-04-21 13:13:50', NULL, 2);
INSERT INTO `sys_auth` VALUES (1252465645007650818, '出库单', '/wms/output_wms', 1, 'icon-clipboard-list', 1252465159240138754, 'PC', '', 'admin', 'admin', '2020-04-21 13:14:14', '2020-04-23 16:58:34', NULL, 1);
INSERT INTO `sys_auth` VALUES (1252465832732114946, '出库审批', '/wms/output_approve', 1, 'icon-clipboard-check', 1252465159240138754, 'PC', '', 'admin', 'admin', '2020-04-21 13:14:58', '2020-04-21 13:14:58', NULL, 2);
INSERT INTO `sys_auth` VALUES (1252465961874735106, '历史查询', '/wms_history', 1, 'icon-history', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-21 13:15:29', '2020-04-26 10:45:56', NULL, 10);
INSERT INTO `sys_auth` VALUES (1252466279891058689, '库存日志', '/wms/inventory_history', 1, 'icon-folder1', 1252465961874735106, 'PC', '', 'admin', 'admin', '2020-04-21 13:16:45', '2020-04-21 13:17:38', NULL, 1);
INSERT INTO `sys_auth` VALUES (1252466700554584065, '入库日志', '/wms/receipt_detail_history', 1, 'icon-folder1', 1252465961874735106, 'PC', '', 'admin', 'admin', '2020-04-21 13:18:25', '2020-04-24 16:38:15', NULL, 2);
INSERT INTO `sys_auth` VALUES (1252466825741975554, '出库日志', '/wms/output_detail_history', 1, 'icon-folder1', 1252465961874735106, 'PC', '', 'admin', 'admin', '2020-04-21 13:18:55', '2020-04-24 16:38:15', NULL, 3);
INSERT INTO `sys_auth` VALUES (1252466915126788097, '移库日志', '/wms/transfer_history', 1, 'icon-folder1', 1252465961874735106, 'PC', '', 'admin', 'admin', '2020-04-21 13:19:16', '2020-04-27 11:30:31', NULL, 5);
INSERT INTO `sys_auth` VALUES (1253191437358407682, '入库完成', '/wms/receipt_finish', 1, 'icon-box-open', 1252465059818356737, 'PC', '', 'admin', 'admin', '2020-04-23 13:18:16', '2020-04-24 14:32:21', NULL, 3);
INSERT INTO `sys_auth` VALUES (1253570424987365377, '出库完成', '/wms/output_finish', 1, 'icon-truck-loading', 1252465159240138754, 'PC', '', 'admin', 'admin', '2020-04-24 14:24:14', '2020-04-24 14:32:21', NULL, 3);
INSERT INTO `sys_auth` VALUES (1254240673361530882, '移库', '/wms/trans', 1, 'icon-exchange-alt', 1250946156956946433, 'PC', '', 'admin', 'admin', '2020-04-26 10:47:33', '2020-04-26 10:50:28', NULL, 6);
INSERT INTO `sys_auth` VALUES (1254240799157096450, '移库单', '/wms/transfer', 1, 'icon-clipboard-list', 1254240673361530882, 'PC', '', 'admin', 'admin', '2020-04-26 10:48:03', '2020-04-26 10:48:03', NULL, 1);
INSERT INTO `sys_auth` VALUES (1254240965608050689, '移库审批', '/wms/transfer_approve', 1, 'icon-clipboard-check', 1254240673361530882, 'PC', '', 'admin', 'admin', '2020-04-26 10:48:43', '2020-04-26 10:48:43', NULL, 2);
INSERT INTO `sys_auth` VALUES (1254241236748832769, '移库完成', '/wms/transfer_finish', 1, 'icon-exchange-alt', 1254240673361530882, 'PC', '', 'admin', 'admin', '2020-04-26 10:49:48', '2020-04-26 10:49:48', NULL, 3);
INSERT INTO `sys_auth` VALUES (1257920060990246914, '入库单-列表', '/wms/receipt/list', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:08', '2020-05-06 14:28:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920062118514690, '入库单-详情', '/wms/receipt/get', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:08', '2020-05-06 14:28:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920062118514691, '入库单-增改', '/wms/receipt/save', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:08', '2020-05-06 14:28:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920062118514692, '入库单-批改', '/wms/receipt/updateList', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:08', '2020-05-06 14:28:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920062118514693, '入库单-删除', '/wms/receipt/remove', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:08', '2020-05-06 14:28:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920149854965761, '入库明细-列表', '/wms/receipt-detail/list', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:29', '2020-05-06 14:28:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920149863354369, '入库明细-详情', '/wms/receipt-detail/get', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:29', '2020-05-06 14:28:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920149863354370, '入库明细-增改', '/wms/receipt-detail/save', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:29', '2020-05-06 14:28:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920149863354371, '入库明细-批改', '/wms/receipt-detail/updateList', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:29', '2020-05-06 14:28:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920149863354372, '入库明细-删除', '/wms/receipt-detail/remove', 2, 'icon-folder1', 1252465349346967553, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:28:29', '2020-05-06 14:28:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920528734834690, '入库单-提交审批结果', '/wms/receipt/approve', 2, 'icon-folder1', 1252465547934679042, 'PC', '', 'admin', 'admin', '2020-05-06 14:29:59', '2020-05-06 14:31:08', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257920649950220290, '入库单-提交审批', '/wms/receipt/submit', 2, 'icon-folder1', 1252465349346967553, 'PC', '', 'admin', 'admin', '2020-05-06 14:30:28', '2020-05-06 14:30:48', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257921033808728065, '入库单-查看明细报表', '/wms/receipt/reportDetail', 2, 'icon-folder1', 1252465349346967553, 'PC', '', 'admin', 'admin', '2020-05-06 14:32:00', '2020-05-06 14:32:00', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257921164998168578, '入库明细-Excel导入', '/wms/receipt', 2, 'icon-folder1', 1252465349346967553, 'PC', '', 'admin', 'admin', '2020-05-06 14:32:31', '2020-05-06 14:32:31', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257921394153967617, '入库单-完成入库', '/wms/receipt/finish', 2, 'icon-folder1', 1253191437358407682, 'PC', '', 'admin', 'admin', '2020-05-06 14:33:26', '2020-05-06 14:33:26', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922105986076674, '移库单-列表', '/wms/transfer/list', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:15', '2020-05-06 14:36:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922105986076675, '移库单-详情', '/wms/transfer/get', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:15', '2020-05-06 14:36:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922105986076676, '移库单-增改', '/wms/transfer/save', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:15', '2020-05-06 14:36:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922105986076677, '移库单-批改', '/wms/transfer/updateList', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:15', '2020-05-06 14:36:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922105986076678, '移库单-删除', '/wms/transfer/remove', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:15', '2020-05-06 14:36:15', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922226131914754, '移库明细-列表', '/wms/transfer-detail/list', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:44', '2020-05-06 14:36:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922226157080577, '移库明细-详情', '/wms/transfer-detail/get', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:44', '2020-05-06 14:36:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922226157080578, '移库明细-增改', '/wms/transfer-detail/save', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:44', '2020-05-06 14:36:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922226157080579, '移库明细-批改', '/wms/transfer-detail/updateList', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:44', '2020-05-06 14:36:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922226157080580, '移库明细-删除', '/wms/transfer-detail/remove', 2, 'icon-folder1', 1254240799157096450, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:36:44', '2020-05-06 14:36:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922330406506497, '移库单-完成移库', '/wms/transfer/finish', 2, 'icon-folder1', 1254240799157096450, 'PC', '', 'admin', 'admin', '2020-05-06 14:37:09', '2020-05-06 14:37:09', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922429782151170, '移库单-提交审批结果', '/wms/transfer/approve', 2, 'icon-folder1', 1254240965608050689, 'PC', '', 'admin', 'admin', '2020-05-06 14:37:32', '2020-05-06 14:55:07', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257922561139363841, '移库单-提交审批', '/wms/transfer/submit', 2, 'icon-folder1', 1254240799157096450, 'PC', '', 'admin', 'admin', '2020-05-06 14:38:04', '2020-05-06 14:38:04', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927006919536642, '移库单-完成移库', '/wms/transfer/finish', 2, 'icon-folder1', 1254241236748832769, 'PC', '', 'admin', 'admin', '2020-05-06 14:55:44', '2020-05-06 14:55:44', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927202818699265, '出库单-列表', '/wms/output/list', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:30', '2020-05-06 14:56:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927202831282178, '出库单-详情', '/wms/output/get', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:30', '2020-05-06 14:56:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927202831282179, '出库单-增改', '/wms/output/save', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:30', '2020-05-06 14:56:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927202831282180, '出库单-批改', '/wms/output/updateList', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:30', '2020-05-06 14:56:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927202831282181, '出库单-删除', '/wms/output/remove', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:30', '2020-05-06 14:56:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927301825245186, '出库明细-列表', '/wms/output-detail/list', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:54', '2020-05-06 14:56:54', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927301825245187, '出库明细-详情', '/wms/output-detail/get', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:54', '2020-05-06 14:56:54', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927301825245188, '出库明细-增改', '/wms/output-detail/save', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:54', '2020-05-06 14:56:54', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927301825245189, '出库明细-批改', '/wms/output-detail/updateList', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:54', '2020-05-06 14:56:54', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927301825245190, '出库明细-删除', '/wms/output-detail/remove', 2, 'icon-folder1', 1252465645007650818, 'PC', NULL, 'admin', 'admin', '2020-05-06 14:56:54', '2020-05-06 14:56:54', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927447543754754, '出库单-提交审批', '/wms/output/submit', 2, 'icon-folder1', 1252465645007650818, 'PC', '', 'admin', 'admin', '2020-05-06 14:57:29', '2020-05-06 14:57:29', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927564560642050, '出库单-提交审批结果', '/wms/output/approve', 2, 'icon-folder1', 1252465832732114946, 'PC', '', 'admin', 'admin', '2020-05-06 14:57:57', '2020-05-06 14:57:57', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927702419025922, '出库单-完成出库', '/wms/output/finish', 2, 'icon-folder1', 1253570424987365377, 'PC', '', 'admin', 'admin', '2020-05-06 14:58:30', '2020-05-06 14:58:30', NULL, 0);
INSERT INTO `sys_auth` VALUES (1257927894581063681, '出库明细-EXCEL导入', '/wms/output-detail/improtExcel', 2, 'icon-folder1', 1252465645007650818, 'PC', '', 'admin', 'admin', '2020-05-06 14:59:15', '2020-05-06 14:59:15', NULL, 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL,
  `code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码（作为查询 key）',
  `value` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数值',
  `type` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型：1：文本框输入，2：波尔量开关',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1243002276492038145, 'SSO', '0', 'SWITCH', '开启后，同一账号无法在不同地点同时登录。', 'admin', 'admin', '2020-03-26 10:30:10', '2020-03-26 10:41:46', NULL);
INSERT INTO `sys_config` VALUES (1243008009828810754, 'TOKEN_EXPIRE', '99999999', 'INPUT', 'TOKEN过期时间（单位：秒）', 'admin', 'admin', '2020-03-26 10:52:57', '2020-04-16 09:40:32', NULL);
INSERT INTO `sys_config` VALUES (1243008242415550465, 'GET_TRUST', '1', 'SWITCH', '是否信任所有 GET 请求，开启后， GET 请求不再进行权限验证。', 'admin', 'admin', '2020-03-26 10:53:53', '2020-03-26 10:54:40', NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(20) NOT NULL,
  `code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'SYS_AUTH', '权限显示方式', '权限显示方式，用于在快速编辑中的显示方式。', 'admin', 'admin', '2020-03-03 19:33:25', '2020-03-26 09:51:27', NULL);
INSERT INTO `sys_dict` VALUES (2, 'WMS_UNIT', '货品单位', 'WMS系统中的货品单位', 'admin', 'admin', '2020-03-03 19:33:27', '2020-03-25 23:23:46', NULL);
INSERT INTO `sys_dict` VALUES (3, 'SYS_CHANNEL', '渠道', '渠道分类', 'admin', 'admin', '2020-03-26 15:33:16', '2020-03-31 22:25:24', NULL);
INSERT INTO `sys_dict` VALUES (4, 'SYS_INDUSTRY', '行业', '行业分类', 'admin', 'admin', '2020-03-26 15:33:16', '2020-03-31 22:25:24', NULL);
INSERT INTO `sys_dict` VALUES (1243078554716930049, 'SYS_AUTH_CAT', '权限分类', '自定义分类，不参与逻辑', 'admin', 'admin', '2020-03-26 15:33:16', '2020-03-31 22:25:24', NULL);
INSERT INTO `sys_dict` VALUES (1251061535586787329, 'WMS_COMPANY_CAT', 'WMS系统合作伙伴分类', '', 'admin', 'admin', '2020-04-17 16:14:48', '2020-04-17 16:14:48', NULL);

-- ----------------------------
-- Table structure for sys_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail`  (
  `id` bigint(20) NOT NULL,
  `dict_id` bigint(20) NULL DEFAULT NULL COMMENT '字典id',
  `dict_code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类编号',
  `code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `color` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '颜色',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  `remark` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `rank` int(255) NULL DEFAULT 1 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES (2, 2, 'WMS_UNIT', 'MM', '毫米', NULL, 'admin', '2020-03-23 21:11:15', 'admin', '2020-03-25 21:54:30', NULL, '公制毫米单位', 1);
INSERT INTO `sys_dict_detail` VALUES (1242739094712188930, 2, 'WMS_UNIT', 'GE', '个', NULL, 'admin', '2020-03-25 17:04:23', 'admin', '2020-03-25 20:12:29', NULL, '123123213123', 1);
INSERT INTO `sys_dict_detail` VALUES (1242743813513904129, 1, 'SYS_AUTH', 'INPUT', '文本编辑', NULL, 'admin', '2020-03-25 17:23:08', 'admin', '2020-03-26 11:11:42', NULL, '在表格编辑中使用文本编辑框', 1);
INSERT INTO `sys_dict_detail` VALUES (1242998261939142658, 1, 'SYS_AUTH', 'SWITCH', '开关编辑', NULL, 'admin', '2020-03-26 10:14:14', 'admin', '2020-03-26 11:11:55', NULL, '在表格编辑中使用 switch 开关', 1);
INSERT INTO `sys_dict_detail` VALUES (1243370303142178818, 1243078554716930049, 'SYS_AUTH_CAT', 'PC', 'PC端', '#00B5FF', 'admin', '2020-03-27 10:52:34', NULL, NULL, NULL, '', 1);
INSERT INTO `sys_dict_detail` VALUES (1243371046691614721, 1243078554716930049, 'SYS_AUTH_CAT', 'MOBILE', '移动端', '#0D94AA', 'admin', '2020-03-27 10:55:32', NULL, NULL, NULL, '', 1);
INSERT INTO `sys_dict_detail` VALUES (1243371046691614722, 3, 'SYS_CHANNEL', 'c1', '头条', '#0D94AA', 'admin', '2020-03-27 10:55:32', NULL, NULL, NULL, '', 1);
INSERT INTO `sys_dict_detail` VALUES (1251061871126913025, 1251061535586787329, 'WMS_COMPANY_CAT', 'GYS', '供应商', '#2D8CF0', 'admin', '2020-04-17 16:16:08', 'admin', '2020-04-17 16:18:03', NULL, '', 1);
INSERT INTO `sys_dict_detail` VALUES (1251061971718905857, 1251061535586787329, 'WMS_COMPANY_CAT', 'KH', '客户', '#19BE6B', 'admin', '2020-04-17 16:16:32', 'admin', '2020-04-17 16:16:32', NULL, '', 1);
INSERT INTO `sys_dict_detail` VALUES (1251062310819995650, 1251061535586787329, 'WMS_COMPANY_CAT', 'CYS', '承运商', '#FF9900', 'admin', '2020-04-17 16:17:53', 'admin', '2020-04-17 16:17:53', NULL, '', 1);

-- ----------------------------
-- Table structure for sys_request_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_request_log`;
CREATE TABLE `sys_request_log`  (
  `id` bigint(20) NOT NULL COMMENT '雪花主键',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `api` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问API',
  `params` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数JSON',
  `response` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回参数',
  `success` tinyint(255) NULL DEFAULT 0 COMMENT '是否成功',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '编辑时间',
  `create_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编辑人',
  `tenant_id` bigint(20) NULL DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `pid` bigint(20) NULL DEFAULT NULL COMMENT '父id',
  `remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1257941013026013186, '来宾用户', NULL, '', 'admin', 'admin', '2020-05-06 15:51:23', '2020-05-06 15:51:23', NULL);

-- ----------------------------
-- Table structure for sys_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_auth`;
CREATE TABLE `sys_role_auth`  (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  `auth_id` bigint(20) NULL DEFAULT NULL,
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_auth
-- ----------------------------
INSERT INTO `sys_role_auth` VALUES (1247359872225226754, 1244901910545231873, 1244969571338407937, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872275558402, 1244901910545231873, 1245258597996150786, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872275558403, 1244901910545231873, 1244973963013771266, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872275558404, 1244901910545231873, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947009, 1244901910545231873, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947010, 1244901910545231873, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947011, 1244901910545231873, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947012, 1244901910545231873, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947013, 1244901910545231873, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947014, 1244901910545231873, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947015, 1244901910545231873, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947016, 1244901910545231873, 1244893434272083970, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947017, 1244901910545231873, 1245531681860866050, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947018, 1244901910545231873, 1245532582025613314, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947019, 1244901910545231873, 1245538737795862530, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872283947020, 1244901910545231873, 1245540007948554242, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872296529921, 1244901910545231873, 1245982072926879745, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872296529922, 1244901910545231873, 1245982383368290306, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1247359872296529923, 1244901910545231873, 1245982496740327425, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333506, 1245219177809031169, 1248061837909385217, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333507, 1245219177809031169, 1248061373381828610, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333508, 1245219177809031169, 1248061424950796290, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333509, 1245219177809031169, 1248061540122189826, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333510, 1245219177809031169, 1248061604844494849, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333511, 1245219177809031169, 1248061685660344321, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333512, 1245219177809031169, 1248061892020101122, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333513, 1245219177809031169, 1248061960609554434, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333514, 1245219177809031169, 1248062044613074946, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333515, 1245219177809031169, 1248062162493988865, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333516, 1245219177809031169, 1248060545086480385, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333517, 1245219177809031169, 1248060633355608066, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333518, 1245219177809031169, 1248060723482812418, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333519, 1245219177809031169, 1248060795557732354, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333520, 1245219177809031169, 1248060865019600897, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333521, 1245219177809031169, 1248067836858818562, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333522, 1245219177809031169, 1248067715752484865, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333523, 1245219177809031169, 1248067633779007490, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333524, 1245219177809031169, 1248067588979646465, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333525, 1245219177809031169, 1248067455227486210, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333526, 1245219177809031169, 1248067386604478466, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382269333527, 1245219177809031169, 1248066623975153665, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722113, 1245219177809031169, 1248066563463929858, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722114, 1245219177809031169, 1248066517012013057, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722115, 1245219177809031169, 1248066464939728898, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722116, 1245219177809031169, 1248066409121931266, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722117, 1245219177809031169, 1248066339571982338, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722118, 1245219177809031169, 1248066208646782978, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722119, 1245219177809031169, 1248057803618082817, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722120, 1245219177809031169, 1248058106190979073, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722121, 1245219177809031169, 1248058247186702337, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722122, 1245219177809031169, 1248058416749830146, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722123, 1245219177809031169, 1248058504414978049, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722124, 1245219177809031169, 1248067015702175745, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722125, 1245219177809031169, 1248066969862627329, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722126, 1245219177809031169, 1248066909967966210, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722127, 1245219177809031169, 1248066836609589250, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722128, 1245219177809031169, 1248066758067052546, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722129, 1245219177809031169, 1248058726125887489, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722130, 1245219177809031169, 1248058852361854977, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722131, 1245219177809031169, 1248058917147074562, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722132, 1245219177809031169, 1248059046017064961, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722133, 1245219177809031169, 1248059172437581825, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722134, 1245219177809031169, 1248063958272663554, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722135, 1245219177809031169, 1248064026304274434, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722136, 1245219177809031169, 1248064135482007554, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722137, 1245219177809031169, 1248064192205774850, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722138, 1245219177809031169, 1248064233117016065, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722139, 1245219177809031169, 1248063161593978881, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722140, 1245219177809031169, 1248063236206452738, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722141, 1245219177809031169, 1248063296461824002, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722142, 1245219177809031169, 1248063355664424961, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722143, 1245219177809031169, 1248063478075187202, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722144, 1245219177809031169, 1248063588427325442, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722145, 1245219177809031169, 1248063645583106050, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722146, 1245219177809031169, 1248063703217037313, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382277722147, 1245219177809031169, 1248063767587020801, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110722, 1245219177809031169, 1248063852177743873, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110723, 1245219177809031169, 1248064478018232321, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110724, 1245219177809031169, 1248064535396311042, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110725, 1245219177809031169, 1248064689037860865, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110726, 1245219177809031169, 1248064742745923585, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110727, 1245219177809031169, 1248064806230908930, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110728, 1245219177809031169, 1248065306804314114, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110729, 1245219177809031169, 1248065398013648897, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110730, 1245219177809031169, 1248065453831446529, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110731, 1245219177809031169, 1248065507111690241, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110732, 1245219177809031169, 1248065566628864001, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110733, 1245219177809031169, 1248065627005870082, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110734, 1245219177809031169, 1248065690323083265, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110735, 1245219177809031169, 1248059802841473025, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110736, 1245219177809031169, 1248059906818269185, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110737, 1245219177809031169, 1248060023034044417, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110738, 1245219177809031169, 1248060107545075713, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110739, 1245219177809031169, 1248060175111118850, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110740, 1245219177809031169, 1248060275388538881, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110741, 1245219177809031169, 1248060352026861569, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110742, 1245219177809031169, 1248065018009706498, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110743, 1245219177809031169, 1248059476121968641, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110744, 1245219177809031169, 1248065222242951170, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110745, 1245219177809031169, 1248059606237667329, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110746, 1245219177809031169, 1248059697107263489, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110747, 1245219177809031169, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110748, 1245219177809031169, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110749, 1245219177809031169, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110750, 1245219177809031169, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110751, 1245219177809031169, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1248068382286110752, 1245219177809031169, 1244893434272083970, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038707736577, 1257941013026013186, 1, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038720319490, 1257941013026013186, 2, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038720319491, 1257941013026013186, 4, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513794, 1257941013026013186, 3, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513795, 1257941013026013186, 5, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513796, 1257941013026013186, 6, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513797, 1257941013026013186, 7, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513798, 1257941013026013186, 8, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513799, 1257941013026013186, 1244893434272083970, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513800, 1257941013026013186, 1250946156956946433, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513801, 1257941013026013186, 1250946376805584897, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513802, 1257941013026013186, 1250946729915650049, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513803, 1257941013026013186, 1250946807938093058, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513804, 1257941013026013186, 1250946903194931201, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513805, 1257941013026013186, 1251042062280654849, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513806, 1257941013026013186, 1251042189653278722, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513807, 1257941013026013186, 1251042711470833666, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513808, 1257941013026013186, 1252464273684152322, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513809, 1257941013026013186, 1252464537463930881, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513810, 1257941013026013186, 1252464646419365889, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513811, 1257941013026013186, 1252465059818356737, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513812, 1257941013026013186, 1252465349346967553, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513813, 1257941013026013186, 1252465547934679042, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513814, 1257941013026013186, 1253191437358407682, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513815, 1257941013026013186, 1252465159240138754, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513816, 1257941013026013186, 1252465645007650818, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513817, 1257941013026013186, 1252465832732114946, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513818, 1257941013026013186, 1253570424987365377, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513819, 1257941013026013186, 1254240673361530882, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513820, 1257941013026013186, 1254240799157096450, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038724513821, 1257941013026013186, 1254240965608050689, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456833, 1257941013026013186, 1254241236748832769, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456834, 1257941013026013186, 1252465961874735106, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456835, 1257941013026013186, 1252466279891058689, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456836, 1257941013026013186, 1252466700554584065, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456837, 1257941013026013186, 1252466825741975554, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456838, 1257941013026013186, 1252466915126788097, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456839, 1257941013026013186, 1248438071659020289, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456840, 1257941013026013186, 1248438375238549506, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);
INSERT INTO `sys_role_auth` VALUES (1257941038766456841, 1257941013026013186, 1248438586342064130, 'admin', 'admin', '2020-05-06 15:51:29', '2020-05-06 15:51:29', NULL);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组名',
  `code` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES (1, '系统管理组', 'ADMIN', '系统管理员', 'admin', 'admin', '2020-03-18 17:22:48', '2020-05-06 15:49:55', NULL);
INSERT INTO `sys_tenant` VALUES (1257940794225950721, '来宾查看', 'GUEST', '', 'admin', 'admin', '2020-05-06 15:50:31', '2020-05-06 15:50:31', NULL);

-- ----------------------------
-- Table structure for sys_upload
-- ----------------------------
DROP TABLE IF EXISTS `sys_upload`;
CREATE TABLE `sys_upload`  (
  `id` bigint(20) NOT NULL,
  `image` tinyint(4) NULL DEFAULT 0 COMMENT '是否为图片',
  `url` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `suffix` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `biz_code` tinyint(4) NULL DEFAULT NULL COMMENT '业务编码：\n1：CMS系统附件',
  `org_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原始名称',
  `create_user` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_upload
-- ----------------------------
INSERT INTO `sys_upload` VALUES (1229242093869625345, 0, 'https://fangjc.oss-cn-shanghai.aliyuncs.com/20200217_489b78a3-3733-402c-8b6e-eceb0534278f.png', '.png', 1, 'Capture001.png', 'admin', NULL, NULL, '2020-02-17 11:12:08', NULL);
INSERT INTO `sys_upload` VALUES (1229246154429825026, 0, 'https://fangjc.oss-cn-shanghai.aliyuncs.com/20200217_ecd0ff26-85cd-4f3f-ac49-54a27d45efbf.xlsx', '.xlsx', 1, 'JMY20190408-1i（森鸿）.xlsx', 'admin', NULL, NULL, '2020-02-17 11:28:15', NULL);
INSERT INTO `sys_upload` VALUES (1229684085162385409, 1, 'https://fangjc.oss-cn-shanghai.aliyuncs.com/20200218_57ca731b-80b7-41c0-8571-a5781d5e7b01.jpg', '.jpg', 1, 'u=4156780790,1776577550&fm=26&gp=0.jpg', 'admin', NULL, '2020-02-18 16:28:26', NULL, NULL);
INSERT INTO `sys_upload` VALUES (1245602283808231426, 1, 'https://fangjc.oss-cn-shanghai.aliyuncs.com/20200402_2c53c87f-620d-4fa5-bd23-b0022fb16f46.png', '.png', 1, '微信图片_20200402094304.png', 'admin', NULL, '2020-04-02 14:41:40', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名（登录用）',
  `real_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `nick_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `password` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码（加密后的）',
  `salt` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码盐值',
  `age` int(11) NULL DEFAULT 0 COMMENT '年龄',
  `gender` tinyint(4) NULL DEFAULT NULL COMMENT '性别：1：女，2：男；',
  `type` tinyint(4) NULL DEFAULT 1 COMMENT '用户类型：1：普通用户；2：后台用户；10：root 账号',
  `id_card` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `phone` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '座机电话',
  `mobile` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动电话',
  `address` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系地址',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `token` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'token 登录凭证',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  `root` tinyint(4) NULL DEFAULT 0 COMMENT '是否为 root 用户',
  `activated` tinyint(4) NULL DEFAULT 1 COMMENT '是否激活',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '', '爱编程的老傻瓜', '0547e80c4efb20797bca8c1207dfdd3e', '37N6IO', NULL, 2, 2, NULL, NULL, '', NULL, '0000-00-00', 'KNaEx2xihUt5QLESPFKd6WWoMnC6RLKAV3HGFvK+dp86p65y90qqd9aNLSflSW2GSOONGqjFriwmPqaZ+ApmgQ==', NULL, NULL, 1, 1, 'admin', 'admin', '2020-03-10 20:53:13', '2020-05-10 09:10:10');
INSERT INTO `sys_user` VALUES (1257940506744160257, 'guest', '', '', 'ba3624c2283e2fe9577d34401141ab9c', '8PB394', 0, NULL, 2, '', '', '', '', NULL, 'ZLRvYgdJ3KdmlA6HxVjdCb28bKudfRnCzc7KzVT0vpCUfvv8yScMhRxciaVwbGFYEo6x4BYyJ1JQTh3zsyUHYXADJtRWjh0WV2DYuqlR8I8=', '', NULL, 0, 1, 'admin', 'guest', '2020-05-06 15:49:22', '2020-05-09 22:50:27');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1257941128889466882, 1257940506744160257, 1257941013026013186, 'admin', 'admin', '2020-05-06 15:51:51', '2020-05-06 15:51:51', 1257940794225950721);

-- ----------------------------
-- Table structure for sys_user_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_tenant`;
CREATE TABLE `sys_user_tenant`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `tenant_id` bigint(20) NULL DEFAULT NULL,
  `def` tinyint(4) NULL DEFAULT 0 COMMENT '是否默认',
  `create_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_user` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_tenant
-- ----------------------------
INSERT INTO `sys_user_tenant` VALUES (1248074111717957635, 1245163833116282882, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_tenant` VALUES (1248074111717957636, 1245163833116282882, 1248072149974867970, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user_tenant` VALUES (1257941129032073218, 1257940506744160257, 1257940794225950721, 0, 'admin', 'admin', '2020-05-06 15:51:51', '2020-05-06 15:51:51');

SET FOREIGN_KEY_CHECKS = 1;
